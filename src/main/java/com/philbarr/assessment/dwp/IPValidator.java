package com.philbarr.assessment.dwp;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Class for validating a IP addresses. Each octet must be between 0 and 255, there must
 * be four octets, and each octet can have leading zeros.
 * @author philbarr
 */
public class IPValidator {

	/**
	 * Test that a string can be divided into 4 sections 
	 * by a dot
	 * @param ipaddress
	 * @return true if the string can be divided into four, false otherwise
	 */
	public boolean hasFourOctets(String ipaddress) {
		if (ipaddress == null) {
			return false;
		}
		if (ipaddress.equals("")) {
			return false;
		}
		if (ipaddress.contains("..")) {
			return false;
		}
		if (ipaddress.split("\\.").length == 4) {
			return true;
		}
		return false;
	}

	/**
	 * Test that each octet of an IP address is between zero and 255, allowing
	 * for leading zeros.
	 * @param ipaddress
	 * @return true if a string split by a dot results in an array of numbers between 0 and 255
	 */
	public boolean eachOctetBetweenZeroAnd255(String ipaddress) {
		String[] octets = ipaddress.split("\\.");
		boolean allOctetsTruth = true;
		for (int i = 0; i < octets.length; i++) {
			try {
				int octetInt = Integer.parseInt(octets[i]);
				if (octetInt >= 0 && octetInt <= 255) {
					allOctetsTruth &= true;
				} else {
					allOctetsTruth = false;
				}
			} catch (NumberFormatException e) {
				allOctetsTruth = false;
			}
		}
		return allOctetsTruth;
	}

	/**
	 * Validates a JSONArray of strings to be IP Addresses
	 * @param ips
	 * @return true if all objects in the array are strings that represent IP Addresses 
	 * as defined by the class comment.
	 */
	public JSONArray validate(JSONArray ips) {
		JSONArray output = new JSONArray();
		for (int i = 0; i < ips.length(); i++) {
			try {
				String ip = ips.getString(i);
				if (hasFourOctets(ip) && eachOctetBetweenZeroAnd255(ip)) {
					output.put(true);
				} else {
					output.put(false);
				}
			} catch (JSONException e) {
				output.put(false);
			}
		}
		return output;
	}
}
