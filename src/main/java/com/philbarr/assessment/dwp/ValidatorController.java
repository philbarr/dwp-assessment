package com.philbarr.assessment.dwp;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ValidatorController {
	
	/**
	 * Validates JSON input of IP Addresses.
	 * This method results in:
	 * 200 - if the input can be parsed as JSON into a JSON Array.
	 * 400 - if the input is not a valid JSON Array.
	 * 500 - if anything else bad happens.
	 * @param json
	 * @return a list of true/false values if each IP is valid
	 */
	@RequestMapping(method=RequestMethod.POST, path="/validator", produces = "application/json", consumes="application/json")
	public ResponseEntity<String> validator(@RequestBody String json) {
		IPValidator validator = new IPValidator();
		JSONArray validation = new JSONArray();
		try {
			validation = validator.validate(new JSONArray(json));
		} catch (JSONException e) {
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		} catch (Exception ex) {
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<String>(validation.toString(), HttpStatus.OK);
	}
}
