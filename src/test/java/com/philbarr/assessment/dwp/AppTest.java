package com.philbarr.assessment.dwp;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
@AutoConfigureMockMvc
public class AppTest {
	@Autowired
    private MockMvc mvc;
	
	@Test
    public void example_specification_passes() throws Exception {
        mvc.perform(post("/validator")
        	.contentType(MediaType.APPLICATION_JSON)
        	.content("[\"123.12.012.1\", \"123.456.789.012\", \"hello.there.mr.smith\", \"11.22.33.44.55\", \"1.2.3.4\"]"))
            .andExpect(status().isOk())
            .andExpect(content().json("[true, false, false, false, true]")).andDo(MockMvcResultHandlers.print());
    }
	
	@Test
	public void badly_formed_json_returns_400() throws Exception {
		mvc.perform(post("/validator")
	        	.contentType(MediaType.APPLICATION_JSON)
	        	.content("\"123.12.012.1\", \"123.456.789.012\", \"hello.there.mr.smith\", \"11.22.33.44.55\", \"1.2.3.4\"]"))
	            .andExpect(status().is4xxClientError());
	}
}
