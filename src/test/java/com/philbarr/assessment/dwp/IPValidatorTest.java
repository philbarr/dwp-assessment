package com.philbarr.assessment.dwp;

import static org.junit.Assert.*;

import org.hamcrest.core.Is;
import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

public class IPValidatorTest {

	private IPValidator validator;

	@Before
	public void setup() {
		validator = new IPValidator();
	}
	
	@Test
	public void ip_has_four_octets() {
		assertFalse(validator.hasFourOctets(null));
		assertFalse(validator.hasFourOctets(""));
		assertFalse(validator.hasFourOctets("123...123"));
		assertTrue(validator.hasFourOctets("123.234.345.346")); 
	}

	@Test
	public void each_octet_between_zero_and_255() {
		assertTrue(validator.eachOctetBetweenZeroAnd255("0.0.0.0"));
		assertTrue(validator.eachOctetBetweenZeroAnd255("255.255.255.255"));
		assertFalse(validator.eachOctetBetweenZeroAnd255("256.256.256.256"));
		assertFalse(validator.eachOctetBetweenZeroAnd255("255.255.256.256"));
		assertFalse(validator.eachOctetBetweenZeroAnd255("asd.sdf.fff.sss"));
	}
	
	@Test
	public void each_octet_can_have_leading_zeros() {
		assertTrue(validator.eachOctetBetweenZeroAnd255("000.0.00.00"));
		assertTrue(validator.eachOctetBetweenZeroAnd255("1.01.001.111"));
	}

	@Test
	public void json_array_of_ips_are_valid() throws JSONException {
		JSONArray input = new JSONArray();
		input.put("123.12.012.1");
		input.put("123.456.789.012");
		input.put("hello.there.mr.smith");
		input.put("11.22.33.44.55");
		input.put("1.2.3.4");
		
		JSONArray expected = new JSONArray();
		expected.put(true);
		expected.put(false);
		expected.put(false);
		expected.put(false);
		expected.put(true);
		JSONArray actual = validator.validate(input);
		JSONAssert.assertEquals(expected, actual, true);
	}
}
